# CallSDK

[![CI Status](https://img.shields.io/travis/Tarek Hteit/CallSDK.svg?style=flat)](https://travis-ci.org/Tarek Hteit/CallSDK)
[![Version](https://img.shields.io/cocoapods/v/CallSDK.svg?style=flat)](https://cocoapods.org/pods/CallSDK)
[![License](https://img.shields.io/cocoapods/l/CallSDK.svg?style=flat)](https://cocoapods.org/pods/CallSDK)
[![Platform](https://img.shields.io/cocoapods/p/CallSDK.svg?style=flat)](https://cocoapods.org/pods/CallSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CallSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CallSDK'
```

## Author

Tarek Hteit, tarek.hteit@inmobiles.net

## License

CallSDK is available under the MIT license. See the LICENSE file for more info.
