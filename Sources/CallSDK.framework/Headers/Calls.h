//
//  Calls.h
//  GlobalChat
//
//  Created by nachkar on 3/23/16.
//  Copyright © 2016 Noel Achkar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Users;

NS_ASSUME_NONNULL_BEGIN

@interface Calls : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+ (Calls *)insertInManagedObjectContext:(NSManagedObjectContext *)moc uuid:(NSString *)uuid statusCode:(NSString *)statusCode phoneNumber:(NSString *)phoneNumber createdDate:(NSDate *)createdDate endDate:(NSDate *)endDate name:(NSString *)name;
+ (Calls *)updateCallCreatedDate:(NSString *)phoneNumber startDate:(NSDate *)startDate
      inManagedObjectContext:(NSManagedObjectContext *)moc;
+ (Calls *)updateCallEndDate:(NSString *)phoneNumber endDate:(NSDate *)endDate duration:(NSNumber *)duration
      inManagedObjectContext:(NSManagedObjectContext *)moc;
+ (Calls *)updateCallStatus:(NSString *)phoneNumber
     inManagedObjectContext:(NSManagedObjectContext *)moc withStatus:(NSString *)status;
+ (void)deleteInManagedObjectContext:(NSManagedObjectContext *)moc
                            withItem:(Calls *)call;
+(void)deleteCallInManagedObjectContext:(NSManagedObjectContext *)moc withCall:(Calls *)call;
+ (NSSet *)fetchCallByNumber:(NSString *)number
      inManagedObjectContext:(NSManagedObjectContext *)moc;
+ (void)deleteAllCallsInMoc:(NSManagedObjectContext *)context;
+ (NSSet *)fetchAllCallsinManagedObjectContext:(NSManagedObjectContext *)moc;
@end

NS_ASSUME_NONNULL_END

#import "Calls+CoreDataProperties.h"
