//
//  Calls+CoreDataProperties.h
//  GlobalChat
//
//  Created by nachkar on 3/23/16.
//  Copyright © 2016 Noel Achkar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Calls.h"
#import "ContactsDB.h"
NS_ASSUME_NONNULL_BEGIN

@interface Calls (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *callResult;
@property (nullable, nonatomic, retain) NSDate *createdDate;
@property (nullable, nonatomic, retain) NSDate *endDate;
@property (nullable, nonatomic, retain) NSString *phoneNumber;
@property (nullable, nonatomic, retain) NSString *status;
@property (nullable, nonatomic, retain) ContactsDB *isFavorite;
@property (nonatomic, retain) NSNumber *duration;

@end

NS_ASSUME_NONNULL_END
