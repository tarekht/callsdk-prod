//
//  Profile.h
//
//  Created by Noel Achkar on 3/24/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ProfileGlobal : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *dateOfBirth;
@property (nonatomic, strong) NSString *xmppDomain;
@property (nonatomic, strong) NSString *xmppPass;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *idUser;
@property (nonatomic, strong) NSString *xmppPort;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *mSISDN;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *expiryDate;

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *usedBalancePercentage;
@property (nonatomic, strong) NSString *totalBalance;
@property (nonatomic, strong) NSString *numberOfDaysRemaining;
@property (nonatomic, strong) NSString *numberOfMinutesRemaining;
@property (nonatomic, strong) NSString *numberOfMinutesUsed;
@property (nonatomic, strong) NSString *isEmailVerified;
@property (nonatomic, strong) NSString *isSubscribed;
@property (nonatomic, strong) NSString *isAutoRenewal;
@property (nonatomic, strong) NSString *isPassportValid;
@property (nonatomic, strong) NSString *isShowCodeStep;
@property (nonatomic, strong) NSString *expiryMessage;
@property (nonatomic, strong) NSString *changeCreditCardLink;
@property (nonatomic, strong) NSString *expiryDateTimestamp;
@property (nonatomic, strong) NSString *buyCreditsLink;
@property (nonatomic, strong) NSString *diasporaNumber;
@property (nonatomic, strong) NSString *isExpired;
@property (nonatomic, strong) NSString *profilePic;
@property (nonatomic, strong) NSString *passportNumber;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *middleName;
@property (nonatomic, strong) NSString *virtualNumber;
@property (nonatomic, strong) NSString *passportPic;
@property (nonatomic, strong) NSString *restComMsisdn;
@property (nonatomic, strong) NSString *restComPass;
@property (nonatomic, strong) NSString *restComDomain;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *ishiddenrefill;
@property (nonatomic, strong) NSString *isPassportOptional;
@property (nonatomic, strong) NSString *isHidePassport;
@property (nonatomic, strong) NSString *isHideCountry;


//Voip
@property (nonatomic, strong) NSString *turnServer;
@property (nonatomic, strong) NSString *turnPort;
@property (nonatomic, strong) NSString *turnUser;
@property (nonatomic, strong) NSString *turnPass;
@property (nonatomic, strong) NSString *stunServer;
@property (nonatomic, strong) NSString *stunPort;
@property (nonatomic, strong) NSString *voipServer;
@property (nonatomic, strong) NSString *voipPort;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
