//
//  Favorites+CoreDataProperties.h
//  GlobalChat
//
//  Created by nachkar on 3/23/16.
//  Copyright © 2016 Noel Achkar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Users.h"

NS_ASSUME_NONNULL_BEGIN

@interface Users (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *imageUrl;
@property (nullable, nonatomic, retain) NSString *firstLetter;
@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *nickname;
@property (nullable, nonatomic, retain) NSString *phoneModel;
@property (nullable, nonatomic, retain) NSString *phoneNumber;
@property (nullable, nonatomic, retain) NSString *thumbUrl;
@property (nullable, nonatomic, retain) NSString *userID;
@property (nullable, nonatomic, retain) NSString *voipPort;
@property (nullable, nonatomic, retain) NSString *voipServer;
@property (nullable, nonatomic, retain) NSString *xmppPort;
@property (nullable, nonatomic, retain) NSString *xmppServer;
@property (nullable, nonatomic, retain) NSString *countryCode;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSString *gender;
@property (nullable, nonatomic, retain) NSString *contactName;
@property (nullable, nonatomic, retain) NSString *dateOfBirth;
@property (nullable, nonatomic, retain) NSString *age;
@property (nullable, nonatomic, retain) NSString *latitude;
@property (nullable, nonatomic, retain) NSString *longitude;
@property (nullable, nonatomic, retain) NSString *fullName;
@property (nullable, nonatomic, retain) NSString *isBlocked;
@property (nullable, nonatomic, retain) NSDate *addedDate;
@property (nullable, nonatomic, retain) NSSet<Calls *> *hasCalls;
@property (nullable, nonatomic, retain) NSString *virtualPhoneNumber;


@end

@interface Users (CoreDataGeneratedAccessors)

- (void)addHasCallsObject:(Calls *)value;
- (void)removeHasCallsObject:(Calls *)value;
- (void)addHasCalls:(NSSet<Calls *> *)values;
- (void)removeHasCalls:(NSSet<Calls *> *)values;

@end

NS_ASSUME_NONNULL_END
