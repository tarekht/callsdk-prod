//
//  Favorites.h
//  GlobalChat
//
//  Created by nachkar on 3/23/16.
//  Copyright © 2016 Noel Achkar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ContactsDB.h"

@class Calls;

NS_ASSUME_NONNULL_BEGIN

@interface Users : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+(Users *)fetchUserById:(NSString *)userId inMoc:(NSManagedObjectContext *)context;
+(Users *)fetchUserByPhoneNumber:(NSString *)phoneNumber inMoc:(NSManagedObjectContext *)context;
+(void)InsertUsersWithId:(NSString *)userId andProfileName:(NSString *)profileName andPhoneNumber:(NSString *)phoneNumber andVirtualNumber:(NSString *)virtualNumber inMoc:(NSManagedObjectContext *)context;
+(void)deleteUserInMoc:(NSManagedObjectContext *)context withUserId:(NSString *)userId;
+(void)deleteAllUsersInMoc:(NSManagedObjectContext *)moc;
+(Users *)insertUserRoy:(ContactsDB *)user inMoc:(NSManagedObjectContext *)context;

+(Users *)blockUsersDBWith:(NSString*)number isBlock:(NSString*)isBlock inMoc:(NSManagedObjectContext *)context ;

@end

NS_ASSUME_NONNULL_END

#import "Users+CoreDataProperties.h"
