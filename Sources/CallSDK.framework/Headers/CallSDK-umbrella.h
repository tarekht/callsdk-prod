#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "ContactsDB.h"
#import "ProfileGlobal.h"
#import "Calls+CoreDataProperties.h"
#import "Calls.h"
#import "Users+CoreDataProperties.h"
#import "Users.h"
#import "LetterAvatarKit.h"
#import "AudioHelper.h"
#import "ColorSpaceUtilities.h"
#import "FileTransferDelegate.h"
#import "IASKAppSettingsViewController.h"
#import "IASKAppSettingsWebViewController.h"
#import "IASKPSSliderSpecifierViewCell.h"
#import "IASKPSTextFieldSpecifierViewCell.h"
#import "IASKPSTitleValueSpecifierViewCell.h"
#import "IASKSettingsReader.h"
#import "IASKSettingsStore.h"
#import "IASKSettingsStoreFile.h"
#import "IASKSettingsStoreUserDefaults.h"
#import "IASKSlider.h"
#import "IASKSpecifier.h"
#import "IASKSpecifierValuesViewController.h"
#import "IASKSwitch.h"
#import "IASKTextField.h"
#import "IASKViewController.h"
#import "LEBackgroundThread.h"
#import "lecore.h"
#import "lelib.h"
#import "LELog.h"
#import "LeNetworkStatus.h"
#import "LogFile.h"
#import "LogFiles.h"
#import "LinphoneAppDelegate.h"
#import "LinphoneCoreSettingsStore.h"
#import "LinphoneIOSVersion.h"
#import "LinphoneManager.h"
#import "Log.h"
#import "OrderedDictionary.h"
#import "ProviderDelegate.h"
#import "Utils.h"

FOUNDATION_EXPORT double CallSDKVersionNumber;
FOUNDATION_EXPORT const unsigned char CallSDKVersionString[];

