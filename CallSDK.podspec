#
# Be sure to run `pod lib lint CallSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CallSDK'
  s.version          = '0.1.0'
  s.summary          = 'CallSDK'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'http://example.com'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Inmobiles' => 'inmobiles.net' }
  s.source           = { :git =>  'https://bitbucket.org/tarekht/callsdk-prod.git', :tag => s.version.to_s }
  s.ios.deployment_target = '10.0'
  s.swift_version    = "5.0"
  s.framework        = 'CallSDK'
  s.vendored_frameworks = 'Sources/CallSDK.framework'

  s.dependency 'SDWebImage'
  s.dependency 'ReachabilitySwift'
  s.dependency 'linphone-sdk/all-frameworks'
end